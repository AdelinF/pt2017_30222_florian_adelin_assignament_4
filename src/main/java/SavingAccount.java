import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.Timer;

/**
 * Created by Adelin on 07-May-17.
 */
public class SavingAccount extends Account {

    public SavingAccount(int id, double balance){
        deposit(balance);
        deposited = true;
        this.id= id;
}


    public void interest(){
        Timer timer = new Timer(5000, new ActionListener() {
            public void actionPerformed(ActionEvent arg0) {
                setInterest();
            }
        });
        timer.setRepeats(true);
        timer.start();
    }
    public void setInterest(){
        this.balance+=this.balance*0.05;
    }
}
