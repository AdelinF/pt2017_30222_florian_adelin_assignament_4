import javax.swing.*;
import java.util.ArrayList;
import java.util.List;


/**
 * Created by Adelin on 07-May-17.
 */
public class Bank implements BankProc {


    public void removePerson(Person p) {
        assert accountsMap.get(p)!= null;

        assert p!=null;
        personsMap.remove(p);
        accountsMap.remove(p);
    }

    public void addPerson(Person p, UserView v) {
        assert accountsMap.get(p)!= null && v!=null && p.getId()>0 && p.getName()!="";

        personsMap.put(p,v);
        accountsMap.put(p, new ArrayList<Account>());
    }

    public void addBalance(Person p, int idAccount, double balance){
        assert accountsMap.get(p)!= null && balance>0 && idAccount>0;

        double newBalance=0;
        List<Account> accounts = accountsMap.get(p);
        Account newAccount=null;
        for(Account account: accounts){
            if(account.id==idAccount){
                newAccount=account;
                if(account instanceof SpendingAccount){
                    newBalance=balance+account.balance;
                    account.deposit(balance);
                    p.notifications.add("Just added "+balance+" to your account with id "+idAccount);

                    if(p.isOn) {
                        Controller.notifyPerson(p);
                    }
                }
                if((account instanceof SavingAccount && !account.deposited)||account.balance==0) {
                    account.deposit(balance);
                    p.notifications.add("Just added "+balance+" to your account with id "+idAccount);

                    if(p.isOn) {
                        Controller.notifyPerson(p);
                    }
                }else if(account.deposited){
                    JOptionPane.showMessageDialog(null,"Already Deposited in this account.");
                }
            }
        }

        assert newBalance==newAccount.balance;
    }

    public void withdraw(Person p, int idAccount, double balance){
        assert accountsMap.get(p)!= null && balance>0 && idAccount>0;

        double newBalance=0;
        Account newAccount=null;
        List<Account> accounts = accountsMap.get(p);
        for(Account account: accounts){
            if(account.id==idAccount){
                if(account instanceof SpendingAccount) {
                    newAccount = account;
                    newBalance = account.balance - balance;
                    account.withdraw(balance);
                    p.notifications.add("Just Withdrawn " + balance + " from your account with id" + idAccount);

                    if (p.isOn) {
                        Controller.notifyPerson(p);
                    }
                }else{
                    account.balance=0;
                    newAccount = account;
                    newBalance = 0;
                    p.notifications.add("Just Withdrawn all from your saving account with id" + idAccount);

                    if (p.isOn) {
                        Controller.notifyPerson(p);
                    }
                }
            }
        }

        assert newBalance==newAccount.balance;
    }

    public void addSpendingAccount(int id, int idAccount, double balance) {
        assert id>0 && idAccount>0 && balance>0;

        Person person = new Person();
        for (Person p : accountsMap.keySet()) {
            if (p.getId() == id) {
                person = p;
            }
        }
        List<Account> accounts = accountsMap.get(person);
        SpendingAccount account = new SpendingAccount(idAccount, balance);
        accounts.add(account);
        person.notifications.add("You have a new Spending account with balance= "+balance);

        if(person.isOn) {
            Controller.notifyPerson(person);
        }
    }

    public void addSavingAccount(int id, int idAccount, double balance) {
        assert id>0 && idAccount>0 && balance>0;

        Person person = new Person();
        for(Person p: accountsMap.keySet()){
            if(p.getId()==id){
                person = p;
            }
        }
        List<Account> accounts = accountsMap.get(person);
        SavingAccount account = new SavingAccount(idAccount,balance);
        account.interest();
        accounts.add(account);
        person.notifications.add("You have a new Saving account with balance= "+balance);

        if(person.isOn) {
            Controller.notifyPerson(person);
        }
    }

    public List<Account> getSpendingAccounts(Person p) {
        List<Account> list = accountsMap.get(p);
        List<Account> listReturned = new ArrayList<Account>();
        for(Account account : list){
            if(account instanceof SpendingAccount){
                listReturned.add(account);
            }
        }
        return listReturned;
    }

    public List<Account> getSavingAccounts(Person p) {
        List<Account> list = accountsMap.get(p);
        List<Account> listReturned = new ArrayList<Account>();
        for(Account account : list){
            if(account instanceof SavingAccount){
                listReturned.add(account);
            }
        }
        return listReturned;
    }

    public void removeAccount(Person p, int  idAccount) {
        assert accountsMap.get(p)!= null && idAccount>0;

        List<Account> list = accountsMap.get(p);
        for(int i=0; i<list.size(); i++){
            if(list.get(i).id == idAccount){
                list.remove(i);
            }
        }
        p.notifications.add("Your account with id" + idAccount+ "has been removed");

        if(p.isOn) {
            Controller.notifyPerson(p);
        }
    }
}
