/**
 * Created by Adelin on 08-May-17.
 */
public class IdFactory {
    private int idPerson;
    private int idAccount;

    public int getIdPerson(){
        return ++idPerson;
    }

    public void setIdPerson(int idPerson) {
        this.idPerson = idPerson;
    }

    public void setIdAccount(int idAccount) {
        this.idAccount = idAccount;
    }

    public int getIdAccount(){
        return ++idAccount;
    }
}
