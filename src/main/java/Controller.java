import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.DecimalFormat;

/**
 * Created by Adelin on 08-May-17.
 */
public class Controller {
    private static final String FILENAME = "C:\\Users\\Adelin\\IdeaProjects\\Tema4\\data.txt";
    private static Bank bank = new Bank();
    private LogInView logIn = new LogInView();
    private AdminView adminView = new AdminView();
    private IdFactory id = new IdFactory();

    public void start(){
        logIn.setVisible(true);
        loadInfo();
        logIn.logInBtn.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                if(logIn.userField.getText().equals("admin")){
                    logInAsAdmin();
                    adminView.setVisible(true);
                }else {
                    for (Person p : bank.personsMap.keySet()) {
                        if (p.getId() == Integer.parseInt(logIn.userField.getText())) {
                            if (p.getPass().equals(logIn.passField.getText())) {
                                logInAsUser(p);
                                p.isOn = true;
                                notifyPerson(p);
                            }
                        }
                    }
                }
            }
        });
    }
    private void logInAsUser(final Person p){
        bank.personsMap.get(p).setVisible(true);
        bank.personsMap.get(p).setTitle(p.getName());
        bank.personsMap.get(p).btnDeposit.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
            bank.addBalance(p,Integer.parseInt(bank.personsMap.get(p).idAccountField.getText()),Double.parseDouble(bank.personsMap.get(p).balanceField.getText()));
            }
        });
        bank.personsMap.get(p).btnLogOut.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                p.isOn=false;
                bank.personsMap.get(p).setVisible(false);
            }
        });
        bank.personsMap.get(p).btnListAccounts.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                JTable table = new JTable();
                table.setModel(accountsToTableModel(p));
                JOptionPane.showMessageDialog(null, new JScrollPane(table));
            }
        });
        bank.personsMap.get(p).btnWithdraw.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                bank.withdraw(p,Integer.parseInt(bank.personsMap.get(p).idAccountField.getText()),Double.parseDouble(bank.personsMap.get(p).balanceField.getText()));
            }
        });

    }

    private void logInAsAdmin(){
        adminView.btnAddPerson.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                Person p = new Person(id.getIdPerson(),adminView.nameField.getText());
                UserView pv = new UserView();
                bank.addPerson(p,pv);
            }
        });

        adminView.btnAddAccount.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                if(adminView.btnSavingAcc.isSelected()){
                    bank.addSavingAccount(Integer.parseInt(adminView.idField.getText()),id.getIdAccount(),Double.parseDouble(adminView.balanceField.getText()));
                }
                if(adminView.btnSpendingAcc.isSelected()){
                    bank.addSpendingAccount(Integer.parseInt(adminView.idField.getText()),id.getIdAccount(),Double.parseDouble(adminView.balanceField.getText()));
                }

            }
        });
        adminView.btnDeleteAccount.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                for(Person p:bank.accountsMap.keySet()) {
                    if(p.getId()==Integer.parseInt(adminView.idField.getText()))
                    bank.removeAccount(p,Integer.parseInt(adminView.idAccountField.getText()));
                }
            }
        });

        adminView.btnDeletePerson.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                Person person = new Person();
                for(Person p : bank.personsMap.keySet()){
                    if(p.getId()==Integer.parseInt(adminView.idField.getText())){
                        person = p;
                    }
                }
                bank.removePerson(person);
            }
        });
        adminView.btnListPersons.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                JTable table = new JTable();
                table.setModel(personsToTableModel());
                JOptionPane.showMessageDialog(null, new JScrollPane(table));
            }
        });
        adminView.btnListAccounts.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                JTable table = new JTable();
                table.setModel(accountsToTableModel());
                JOptionPane.showMessageDialog(null, new JScrollPane(table));
            }
        });
        adminView.btnAddBalance.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                for(Person p: bank.personsMap.keySet()){
                    if(p.getId()==Integer.parseInt(adminView.idField.getText()))
                        bank.addBalance(p,Integer.parseInt(adminView.idAccountField.getText()),Double.parseDouble(adminView.balanceField.getText()));

                }

            }
        });
        adminView.btnSaveInfo.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                saveInfo();
            }
        });

        adminView.btnWithdraw.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                for(Person p: bank.personsMap.keySet()){
                    if(p.getId()==Integer.parseInt(adminView.idField.getText()))
                        bank.withdraw(p,Integer.parseInt(adminView.idAccountField.getText()),Double.parseDouble(adminView.balanceField.getText()));

                }
            }
        });
    }
    private TableModel personsToTableModel() {
        DefaultTableModel model = new DefaultTableModel(
                new Object[] { "Id", "Name","Accounts"}, 0
        );
        for (Person person: bank.accountsMap.keySet()) {
            model.addRow(new Object[]{person.getId(), person.getName(), bank.accountsMap.get(person).size()});

        }
        return model;
    }

    private TableModel accountsToTableModel() {
        DefaultTableModel model = new DefaultTableModel(
                new Object[] { "IdAccount","IdPerson", "Type","Balance"}, 0
        );
        DecimalFormat df = new DecimalFormat("#.##");
        for (Person person: bank.accountsMap.keySet()) {
            for(Account account: bank.accountsMap.get(person)) {
                model.addRow(new Object[]{account.id,person.getId(), account.getClass(), df.format(account.balance)});
            }

        }
        return model;
    }
    private TableModel accountsToTableModel(Person p) {
        DefaultTableModel model = new DefaultTableModel(
                new Object[] { "IdAccount","Your Id", "Type","Balance"}, 0
        );
        DecimalFormat df = new DecimalFormat("#.##");
        for (Person person: bank.accountsMap.keySet()) {
            if(p.getId()==person.getId()) {
                for (Account account : bank.accountsMap.get(person)) {
                    model.addRow(new Object[]{account.id, person.getId(), account.getClass(), df.format(account.balance)});
                }
            }

        }
        return model;
    }

    public static void notifyPerson(Person p){
        while(p.notifications.size()>0){
            try{
                String string = p.notifications.take();
                JOptionPane.showMessageDialog(bank.personsMap.get(p),string);
            }catch (Exception e){
                e.printStackTrace();
            }

        }
    }
    public void loadInfo(){
        BufferedReader br = null;
        FileReader fr = null;
        try {
            fr = new FileReader(FILENAME);
            br = new BufferedReader(fr);
            String sCurrentLine;
            br = new BufferedReader(new FileReader(FILENAME));
            int maxP=0;
            int maxA=0;
            while ((sCurrentLine = br.readLine()) != null) {
                String [] parts = sCurrentLine.split(",");
                String[] aux = parts[0].split(" ");
                Person p = new Person(Integer.parseInt(aux[0]),aux[1]);
                if(p.getId()>maxP){
                    maxP=p.getId();
                }
                bank.addPerson(p,new UserView());
                for(int i=1; i<parts.length; i++){
                    String[] accountData = parts[i].split(" ");
                    if (accountData[1].equals("SavingAccount")) {
                        bank.addSavingAccount(p.getId(), Integer.parseInt(accountData[0]), Double.parseDouble(accountData[2]));
                    } else {
                        bank.addSpendingAccount(p.getId(), Integer.parseInt(accountData[0]), Double.parseDouble(accountData[2]));
                    }
                    if(Integer.parseInt(accountData[0])>maxA){
                        maxA=Integer.parseInt(accountData[0]);
                    }
                }
            }
            id.setIdPerson(maxP);
            id.setIdAccount(maxA);

        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                if (br != null)
                    br.close();
                if (fr != null)
                    fr.close();
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        }
    }

    public void saveInfo(){
        try{
            PrintWriter writer = new PrintWriter("data.txt", "UTF-8");
            String data;
            for(Person p: bank.accountsMap.keySet()){
                data=p.getId()+" "+p.getName()+",";
                for(Account account: bank.accountsMap.get(p)){
                    data+=account.id;
                    if(account instanceof SpendingAccount){
                        data+=" SpendingAccount ";
                        data+=account.balance;
                        data+=",";
                    }else{
                        data+=" SavingAccount ";
                        data+=account.balance;
                        data+=",";
                    }
                }
                writer.println(data);
            }
            writer.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
