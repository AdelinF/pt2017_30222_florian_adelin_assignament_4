import javax.swing.*;
import javax.swing.border.EmptyBorder;

public class AdminView extends JFrame {

    private JPanel contentPane;
    public JTextField nameField;
    public JRadioButton btnSavingAcc = new JRadioButton("SavingAcc");
    public JButton btnAddPerson = new JButton("Add Person");
    public JButton btnAddAccount = new JButton("Add Account");
    public JRadioButton btnSpendingAcc = new JRadioButton("SpendingAcc");
    public JTextField idField = new JTextField();
    public JButton btnDeletePerson = new JButton("Delete Person");
    public JTextField balanceField = new JTextField();
    public JButton btnAddBalance = new JButton("Add Balance");
    public JTextField  idAccountField = new JTextField();
    public JButton btnListPersons = new JButton("List Persons");
    public JButton btnListAccounts = new JButton("List Accounts");
    public JButton btnDeleteAccount = new JButton("Delete Account");
    public JButton btnSaveInfo = new JButton("Save Info");
    public JButton btnWithdraw = new JButton("Withdraw");

    public AdminView() {
        setTitle("Admin View");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setBounds(100, 100, 450, 300);
        contentPane = new JPanel();
        contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
        setContentPane(contentPane);
        contentPane.setLayout(null);

        JPanel panel = new JPanel();
        panel.setBounds(0, 0, 434, 262);
        contentPane.add(panel);
        panel.setLayout(null);

        JLabel lblName = new JLabel("Name:");
        lblName.setBounds(10, 11, 46, 14);
        panel.add(lblName);

        btnAddPerson.setBounds(20, 41, 109, 23);
        panel.add(btnAddPerson);

        nameField = new JTextField();
        nameField.setBounds(66, 8, 86, 20);
        panel.add(nameField);
        nameField.setColumns(10);

        btnAddAccount.setBounds(43, 167, 109, 23);
        panel.add(btnAddAccount);

        btnSpendingAcc.setBounds(6, 137, 109, 23);
        panel.add(btnSpendingAcc);

        btnSavingAcc.setBounds(118, 137, 109, 23);
        panel.add(btnSavingAcc);

        idField.setBounds(92, 110, 86, 20);
        panel.add(idField);
        idField.setColumns(10);

        JLabel lblIdPerson = new JLabel("ID Person:");
        lblIdPerson.setBounds(10, 113, 86, 14);
        panel.add(lblIdPerson);

        btnDeletePerson.setBounds(43, 201, 109, 23);
        panel.add(btnDeletePerson);

        balanceField.setBounds(92, 79, 86, 20);
        panel.add(balanceField);
        balanceField.setColumns(10);

        JLabel lblBalance = new JLabel("Balance:");
        lblBalance.setBounds(10, 82, 46, 14);
        panel.add(lblBalance);


        btnAddBalance.setBounds(162, 239, 109, 23);
        panel.add(btnAddBalance);

        idAccountField.setBounds(66, 240, 86, 20);
        panel.add(idAccountField);
        idAccountField.setColumns(10);

        JLabel lblIdAccount = new JLabel("ID Account");
        lblIdAccount.setBounds(0, 243, 71, 14);
        panel.add(lblIdAccount);

        btnListPersons.setBounds(317, 21, 107, 23);
        panel.add(btnListPersons);

        btnListAccounts.setBounds(317, 55, 107, 23);
        panel.add(btnListAccounts);

        btnDeleteAccount.setBounds(317, 239, 107, 23);
        panel.add(btnDeleteAccount);

        btnSaveInfo.setBounds(317, 205, 107, 23);
        panel.add(btnSaveInfo);

        btnWithdraw.setBounds(162, 205, 109, 23);
        panel.add(btnWithdraw);
    }
}
