/**
 * Created by Adelin on 07-May-17.
 */
public class SpendingAccount extends Account {

    public SpendingAccount(int id, double balance){
        this.id= id;
        deposit(balance);
    }

}
