import javax.swing.*;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;

/**
 * Created by Adelin on 07-May-17.
 */
public class Person {
    private int id;
    private String name;
    private String pass;
    boolean isOn=false;
    public BlockingQueue<String> notifications = new ArrayBlockingQueue(1024);

    public Person(){
    }

    public Person(int id, String name){
        this.id=id;
        this.name=name;
        this.pass=String.valueOf(id*10);
    }


    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getPass() {
        return pass;
    }
}
