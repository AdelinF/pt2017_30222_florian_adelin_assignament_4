/**
 * Created by Adelin on 07-May-17.
 */
public abstract class Account {
    public int id;
    public double balance;
    public boolean deposited = false;

    public void withdraw(double balance){
        this.balance-=balance;
    }
    public void deposit(double balance){
        this.balance+=balance;
    }
}
