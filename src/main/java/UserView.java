/**
 * Created by Adelin on 10-May-17.
 */


import java.awt.BorderLayout;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.JButton;

public class UserView extends JFrame {

    public JPanel contentPane;
    public JTextField idAccountField;
    public JTextField balanceField;
    public JButton btnDeposit = new JButton("Deposit");
    public JButton btnWithdraw = new JButton("Withdraw");
    public JButton btnListAccounts = new JButton("List Accounts");
    public JButton btnLogOut = new JButton("Log Out");

    public UserView() {
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setBounds(100, 100, 450, 300);
        contentPane = new JPanel();
        contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
        contentPane.setLayout(new BorderLayout(0, 0));
        setContentPane(contentPane);

        JPanel panel = new JPanel();
        contentPane.add(panel, BorderLayout.CENTER);
        panel.setLayout(null);

        JLabel lblIdAccount = new JLabel("ID Account");
        lblIdAccount.setBounds(10, 24, 77, 14);
        panel.add(lblIdAccount);

        JLabel lblBalance = new JLabel("Balance");
        lblBalance.setBounds(10, 57, 66, 14);
        panel.add(lblBalance);

        idAccountField = new JTextField();
        idAccountField.setBounds(97, 21, 86, 20);
        panel.add(idAccountField);
        idAccountField.setColumns(10);

        balanceField = new JTextField();
        balanceField.setBounds(97, 54, 86, 20);
        panel.add(balanceField);
        balanceField.setColumns(10);


        btnDeposit.setBounds(10, 96, 89, 23);
        panel.add(btnDeposit);


        btnWithdraw.setBounds(125, 96, 89, 23);
        panel.add(btnWithdraw);


        btnListAccounts.setBounds(73, 140, 110, 23);
        panel.add(btnListAccounts);

        btnLogOut.setBounds(285, 20, 101, 23);
        panel.add(btnLogOut);
    }

}

