import java.util.HashMap;
import java.util.List;

/**
 * Created by Adelin on 07-May-17.
 */
public interface BankProc {
    HashMap<Person,UserView> personsMap = new HashMap<Person,UserView>();
    HashMap<Person,List<Account>> accountsMap = new HashMap<Person,List<Account>>();

    void removePerson(Person p);
    void addPerson(Person p, UserView v);
    void addBalance(Person p, int idAccount, double balance);
    void withdraw(Person p, int idAccount, double balance);
    List<Account> getSavingAccounts(Person a);
    List<Account> getSpendingAccounts(Person a);
    void addSavingAccount(int p, int b, double a);
    void addSpendingAccount(int p, int b,  double a);
    void removeAccount(Person p, int a);

}
